- [ ] Concerns the metadata schema

- [ ] Existing metadata key

- [ ] New metadata key required


## Describe the problem or requirement


## Do you have a proposal already?


## Linked issues

(Please tag potentially related issues)

/label ~MetaData

(Please add other relevant labels if any)
