# Test your metadata validity on the main (master) branch

To avoid any problem when creating a release and interacting with
Zenodo, we propose a small CI snippet to check the validity of your metadata
in a `codemeta.json` file or a `.zenodo.json` file when they are modified
through a merge (pull) request to the main branch of your repository.

## GitLab

Please note that this snippet will only work if you have linked your
gitlab account with Zenodo. If not, please follow this [tutorial](
https://escape-ossr.gitlab.io/eossr/gitlab_to_zenodo.html#link-your-gitlab-project-with-zenodo
)

### Test your `codemeta.json` file

The `eossr-metadata-validator` will test the validity of your `codemeta.json` against ESCAPE OSSR requirements.

```yaml
stages:
 - test

test_codemeta:
  stage: test
  image: gitlab-registry.in2p3.fr/escape2020/wp3/eossr:v1.0
  script:
    - eossr-metadata-validator codemeta.json
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH'
    - changes:
      - codemeta.json
```

### Test your `.zenodo.json` file

You may test the validity of your `.zenodo.json` and its correct upload to Zenodo.

```yaml
stages:
 - test

test_zenodo_upload:
  stage: test
  image: gitlab-registry.in2p3.fr/escape2020/wp3/eossr:v1.0
  script:
    - eossr-zenodo-validator .zenodo.json
    - eossr-check-connection-zenodo --token $ZENODO_TOKEN -p $CI_PROJECT_DIR
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH'
    - changes:
      - .zenodo.json
```

## GitHub

If you are hosting your developments on GitHub, you are probably using [the GitHub-Zenodo integration](https://docs.github.com/en/repositories/archiving-a-github-repository/referencing-and-citing-content) to upload your code to Zenodo when making a GitHub release.

The GitHub-Zenodo integration can use three sources for its metadata:
1. GitHub - if no other source of metadata exist, information such as release title and notes, versioning, contributors, etc... will be provided by GitHub metadata. Metadata from former releases or edited on the Zenodo web portal are **updated**.
2. `CITATION.cff` - if a `CITATION.cff` file lives at the root of your repository, it will be used by Zenodo to **update** existing metadata (from a former release or from GitHub)
3. `.zenodo.json` - if a `.zenodo.json` file lives at the root of your repository, it will be used by Zenodo as **unique** source of metadata and will **replace** all existing metadata

In order to have a unique source of metadata for your project and depending on your workflow, you _might_ want to to convert your `codemeta.json` file to a `.zenodo.json` file before releases using [the `ossr-codemeta2zenodo` CLI](../docstring_sources/eossr_cli/eossr-codemeta2zenodo).

### Test your `codemeta.json` file

You can test the validity of your `codemeta.json` file against the OSSR requirements using the following:

```yaml
name: validate codemeta

on:
  push:
    paths:
      - codemeta.json
  
jobs:
  build:
    runs-on: ubuntu-latest
    container:
      image: gitlab-registry.in2p3.fr/escape2020/wp3/eossr:v1.0
    steps:
      - uses: actions/checkout@v3
      - name: validate codemeta
        run: eossr-metadata-validator codemeta.json
```


### Test your `.zenodo.json` file

A simple validation test can be done using the `eossr-zenodo-validator` CLI.

```yaml
name: validate zenodo

on:
  push:
    paths:
      - .zenodo.json
  
jobs:
  build:
    runs-on: ubuntu-latest
    container:
      image: gitlab-registry.in2p3.fr/escape2020/wp3/eossr:v1.0
    steps:
      - uses: actions/checkout@v3
      - name: validate zenodo
        run: eossr-zenodo-validator .zenodo.json
```

For a more complete test, you can use the `eossr-check-connection-zenodo` CLI
that will try to upload your `.zenodo.json` file to Zenodo and check that the upload was successful.


First, you will need to:
- [create an access token in Zenodo](https://zenodo.org/account/settings/applications/tokens/new/)
(or [sandbox](https://sandbox.zenodo.org/account/settings/applications/tokens/new/)) with a `deposit:write` access
- add it to [your repository secrets](https://docs.github.com/en/actions/security-guides/encrypted-secrets)
under the name `ZENODO_TOKEN`.

Then, you can use the following GitHub action snippet to check the validity of your `.zenodo.json` file.
To do so, add this code snippet in a file `.github/workflows/check_zenodo.yml` in your repository:

```yaml
name: check zenodo publication

on:
  push:
    paths:
      - .zenodo.json

jobs:
  build:
    runs-on: ubuntu-latest
    container:
      image: gitlab-registry.in2p3.fr/escape2020/wp3/eossr:v0.6
    steps:
      - uses: actions/checkout@v3
      - name: check zenodo publication
        run: |
          eossr-check-connection-zenodo --token ${{ secrets.ZENODO_TOKEN }} -p .
```
